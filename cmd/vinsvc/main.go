package main

import (
	"os"
	"time"

	"github.com/labstack/echo"
	emw "github.com/labstack/echo/middleware"
	echoLog "github.com/labstack/gommon/log"
	middleware "github.com/neko-neko/echo-logrus"
	"github.com/neko-neko/echo-logrus/log"
	"github.com/sirupsen/logrus"

	"gitlab.com/remizovm/vinsvc/internal/vinsvc/api"
	"gitlab.com/remizovm/vinsvc/internal/vinsvc/config"
	"gitlab.com/remizovm/vinsvc/internal/vinsvc/storage"
	"gitlab.com/remizovm/vinsvc/internal/vinsvc/vehicleapi"
	"gitlab.com/remizovm/vinsvc/internal/vinsvc/vinprocessor"
)

const bindAddr = ":8080"

func main() {
	cfg := config.Load()

	logger := logrus.New()
	logger.SetLevel(logrus.DebugLevel)

	log.Logger().SetOutput(os.Stdout)
	log.Logger().SetLevel(echoLog.INFO)
	log.Logger().SetFormatter(&logrus.JSONFormatter{
		TimestampFormat: time.RFC3339,
	})

	e := echo.New()

	e.Logger = log.Logger()
	e.Use(middleware.Logger())

	logger.Debug("Initializing the web server and middleware")
	// Middleware
	e.Use(emw.Recover())

	vehicleAPIClient, err := vehicleapi.New(logger, cfg.VehicleAPIEndpoint)
	if err != nil {
		logger.Fatalf("while initializing the Vehicle API client: %v", err)
	}
	// TODO: Send the complete URI here? Like NewMySQL(logger, uri).
	storager, err := storage.NewMySQL(logger, cfg.DBUser, cfg.DBPassword, cfg.DBHost, cfg.DBName, cfg.DBPort)
	if err != nil {
		logger.Fatalf("while initializing the storager: %v", err)
	}

	vinProcessor, err := vinprocessor.New(logger, vehicleAPIClient, storager)
	if err != nil {
		logger.Fatalf("while initializing the vin processor: %v", err)
	}

	bh := api.NewBaseHandler(logger, vinProcessor)

	// Routes
	e.GET("/vins/:vin", bh.VINGet)
	e.POST("/decode", bh.DecodeVINPost)

	logger.Debugf("Listening at %s", cfg.BindAddr)
	// Start server
	logger.Fatal((e.Start(cfg.BindAddr)))
}
