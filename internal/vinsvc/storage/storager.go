package storage

type Storager interface {
	Put(key string, data interface{}) error
	Get(key string) (interface{}, error)
}
