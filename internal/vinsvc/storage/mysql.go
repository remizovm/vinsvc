package storage

import (
	"encoding/json"
	"errors"
	"fmt"

	// MySQL driver support.
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
)

// MySQLStorager implements the storager interface using MySQL.
type MySQLStorager struct {
	logger *logrus.Logger
	db     *sqlx.DB
}

// Put inserts the raw data into the vins table.
func (msqls *MySQLStorager) Put(key string, data interface{}) error {
	raw, err := json.Marshal(data)
	if err != nil {
		return err
	}

	_, err = msqls.db.Exec("INSERT INTO vins (vin, data) VALUES (?, ?);", key, raw)

	return err
}

// Get selects the raw data from the vins table by key (the vin number).
func (msqls *MySQLStorager) Get(key string) (interface{}, error) {
	result := []byte{}

	if err := msqls.db.Get(&result, "SELECT data FROM vins WHERE vin=?;", key); err != nil {
		return nil, err
	}

	return result, nil
}

// NewMySQL creates new MySQLStorager instance.
func NewMySQL(logger *logrus.Logger, user, password, host, dbname string, port int64) (*MySQLStorager, error) {
	if logger == nil {
		return nil, errors.New("logger cant be nil")
	}

	uri := fmt.Sprintf("%s:%s@(%s:%d)/%s?charset=utf8", user, password, host, port, dbname)

	db, err := sqlx.Connect("mysql", uri)
	if err != nil {
		return nil, err
	}

	// Ensure the hardcoded schema (FIXME).
	db.MustExec("CREATE TABLE IF NOT EXISTS vins (vin TEXT, data JSON);")

	return &MySQLStorager{
		logger: logger,
		db:     db,
	}, nil
}
