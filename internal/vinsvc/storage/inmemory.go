package storage

import (
	"sync"

	"github.com/sirupsen/logrus"
)

type InMemStorager struct {
	storage map[string]interface{}
	m       *sync.Mutex
	logger  *logrus.Logger
}

func NewInmemStorager(logger *logrus.Logger) *InMemStorager {
	return &InMemStorager{
		storage: make(map[string]interface{}),
		m:       &sync.Mutex{},
		logger:  logger,
	}
}

func (ims *InMemStorager) Put(key string, data interface{}) error {
	ims.logger.Debugf("ims Put started. key: %s; data: %v", key, data)
	defer ims.logger.Debugf("ims Put ended")

	ims.m.Lock()

	ims.storage[key] = data

	ims.m.Unlock()

	return nil
}

func (ims *InMemStorager) Get(key string) (interface{}, error) {
	ims.logger.Debugf("ims Get started. key: %s", key)
	defer ims.logger.Debugf("ims Get ended")

	ims.m.Lock()
	defer ims.m.Unlock()

	if _, ok := ims.storage[key]; !ok {
		return nil, nil
	}

	return ims.storage[key], nil
}
