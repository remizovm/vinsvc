package api

import (
	"net/http"
	"time"

	"github.com/labstack/echo"

	"gitlab.com/remizovm/vinsvc/internal/vinsvc/models"
)

func (bh *BaseHandler) DecodeVINPost(c echo.Context) error {
	payload := &models.VINRequest{}

	if err := c.Bind(payload); err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	timeout := time.Duration(payload.Timeout) * time.Second

	data, err := bh.vinProcessor.DecodeVIN(payload.VIN, timeout)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, data)
}
