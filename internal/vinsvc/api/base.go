package api

import (
	"github.com/sirupsen/logrus"

	"gitlab.com/remizovm/vinsvc/internal/vinsvc/vinprocessor"
)

type BaseHandler struct {
	logger       *logrus.Logger
	vinProcessor *vinprocessor.VINProcessor
}

func NewBaseHandler(logger *logrus.Logger, vinproc *vinprocessor.VINProcessor) *BaseHandler {
	return &BaseHandler{
		logger:       logger,
		vinProcessor: vinproc,
	}
}
