package api

import (
	"net/http"

	"github.com/labstack/echo"
)

func (bh *BaseHandler) VINGet(c echo.Context) error {
	vin := c.Param("vin")

	if vin == "" {
		return c.JSON(http.StatusBadRequest, "invalid vin")
	}

	data, err := bh.vinProcessor.GetVINInfo(vin)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, data)
}
