package config

import (
	_ "github.com/joho/godotenv/autoload"
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	BindAddr           string `envconfig:"BIND_ADDR", default:":8080"`
	VehicleAPIEndpoint string `envconfig:"VEHICLE_API_ENDPOINT", default:"https://vpic.nhtsa.dot.gov"`
	DBUser             string `envconfig:"DB_USER"`
	DBPassword         string `envconfig:"DB_PASSWORD"`
	DBName             string `envconfig:"DB_NAME"`
	DBHost             string `envconfig:"DB_HOST"`
	DBPort             int64  `envconfig:"DB_PORT" default:"3306"`
}

func Load() *Config {
	cfg := &Config{}

	envconfig.MustProcess("VINSVC", cfg)

	return cfg
}
