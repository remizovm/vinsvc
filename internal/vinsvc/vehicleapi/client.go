package vehicleapi

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/sirupsen/logrus"

	"gitlab.com/remizovm/vinsvc/internal/vinsvc/models"
	"gitlab.com/remizovm/vinsvc/internal/vinsvc/utils"
)

type Client struct {
	logger   *logrus.Logger
	endpoint string
}

func New(logger *logrus.Logger, endpoint string) (*Client, error) {
	return &Client{
		logger:   logger,
		endpoint: endpoint,
	}, nil
}

func (c *Client) DecodeVIN(ctx context.Context, vin string) (*models.VINResponse, error) {
	url := fmt.Sprintf("%s/api//vehicles/DecodeVin/%s?format=json", c.endpoint, vin)
	// Construct the HTTP request
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	vinResp := &models.VINResponse{}
	// Fire the get-with-context func and parse the results. See utils/http.go
	err = utils.GetWithContext(ctx, req, func(resp *http.Response, err error) error {
		if err != nil {
			return err
		}
		defer resp.Body.Close()

		if err := json.NewDecoder(resp.Body).Decode(vinResp); err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		// If we've got a timeout error - wrap it
		if err == context.DeadlineExceeded {
			return nil, errors.New("Timeout")
		}
		return nil, err
	}

	return vinResp, nil
}
