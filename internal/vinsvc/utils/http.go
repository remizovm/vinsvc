package utils

import (
	"context"
	"net/http"
)

// GetWithContext does an HTTP request with the given context and then calls the handler function.
func GetWithContext(ctx context.Context, req *http.Request, f func(*http.Response, error) error) error {
	tr := &http.Transport{}
	client := &http.Client{Transport: tr}

	c := make(chan error, 1)

	go func() {
		c <- f(client.Do(req))
	}()

	select {
	case <-ctx.Done():
		tr.CancelRequest(req)
		<-c
		return ctx.Err()
	case err := <-c:
		return err
	}
}
