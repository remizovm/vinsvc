package vinprocessor

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/sirupsen/logrus"

	"gitlab.com/remizovm/vinsvc/internal/vinsvc/models"
	"gitlab.com/remizovm/vinsvc/internal/vinsvc/storage"
	"gitlab.com/remizovm/vinsvc/internal/vinsvc/vehicleapi"
)

type VINProcessor struct {
	logger        *logrus.Logger
	vehicleClient *vehicleapi.Client
	// A list of "in progress" requests
	inProgressVINList []string
	// Mutex for its protection
	m        *sync.RWMutex
	storager storage.Storager
}

func New(logger *logrus.Logger, vClient *vehicleapi.Client, storager storage.Storager) (*VINProcessor, error) {
	return &VINProcessor{
		logger:            logger,
		vehicleClient:     vClient,
		inProgressVINList: []string{},
		m:                 &sync.RWMutex{},
		storager:          storager,
	}, nil
}

func (vp *VINProcessor) GetVINInfo(vin string) (*models.VINResponse, error) {
	// Lock the list for reading
	vp.m.RLock()

	for _, item := range vp.inProgressVINList {
		if item == vin {
			// If the requested VIN is in the list - return an error
			vp.m.RUnlock()
			return nil, fmt.Errorf("%s is being processed now", vin)
		}
	}

	vp.m.RUnlock()
	// Get the data from the storage
	data, err := vp.storager.Get(vin)
	if err != nil {
		return nil, err
	}

	if data == nil {
		return nil, errors.New("no data found")
	}

	result := &models.VINResponse{}

	if err := json.Unmarshal(data.([]byte), result); err != nil {
		return nil, err
	}

	return result, nil
}

func (vp *VINProcessor) DecodeVIN(vin string, timeout time.Duration) (*models.VINResponse, error) {
	// Lock the list for writing.
	vp.m.Lock()

	for _, item := range vp.inProgressVINList {
		if item == vin {
			// Duplicate request? Return an error.
			vp.m.Unlock()
			return nil, fmt.Errorf("%s is already being processed", vin)
		}
	}
	// Add new VIN to the list.
	vp.inProgressVINList = append(vp.inProgressVINList, vin)
	vp.m.Unlock()
	// Defer the cleanup.
	defer func() {
		// Lock for writing.
		vp.m.Lock()
		defer vp.m.Unlock()

		for i := range vp.inProgressVINList {
			// Find the VIN.
			if vp.inProgressVINList[i] == vin {
				// And delete it.
				vp.inProgressVINList = append(vp.inProgressVINList[:i], vp.inProgressVINList[i+1:]...)
				break
			}
		}
	}()

	// Create the default background context and a cancel function
	ctx, cancel := context.WithCancel(context.Background())
	if timeout != 0 {
		// If we've got any timeout - change the context type
		ctx, cancel = context.WithTimeout(context.Background(), timeout)
	}
	defer cancel()

	result, err := vp.vehicleClient.DecodeVIN(ctx, vin)
	if err != nil {
		return nil, err
	}

	if err := vp.storager.Put(vin, result); err != nil {
		return nil, err
	}

	return result, nil
}
