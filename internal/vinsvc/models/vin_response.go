package models

type VINResponse struct {
	Count          int                `json:"Count"`
	Message        string             `json:"Message"`
	SearchCriteria string             `json:"SearchCriteria"`
	Results        []VINResponseChunk `json:"Results"`
}
