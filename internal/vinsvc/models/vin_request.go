package models

type VINRequest struct {
	VIN     string `json:"vin" form:"vin" query:"vin"`
	Timeout int64  `json:"timeout" form:"timeout" query:"timeout"`
}
