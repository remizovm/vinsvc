package models

type VINResponseChunk struct {
	Value      string `json:"Value"`
	ValueID    string `json:"ValueId"`
	Variable   string `json:"Variable"`
	VariableID int    `json:"VariableId"`
}
